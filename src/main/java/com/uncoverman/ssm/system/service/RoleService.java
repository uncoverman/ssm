package com.uncoverman.ssm.system.service;

import com.uncoverman.ssm.system.domain.Role;
import com.uncoverman.ssm.system.domain.RoleWithMenu;

import java.util.List;

/**
 * Created by NFL on 2018/9/28.
 */
public interface RoleService {


    List<Role> findAllRoles(Role role);

    List<Role> findByName(String roleName);

    List<Role> findUserRole(String username);

    RoleWithMenu findRoleWithMenus(Long roleId);

    void addRole(Role role, Long[] menuIds);

    void updateRole(Role role, Long[] menuIds);

    void deleteRoles(String roleIds);

}
