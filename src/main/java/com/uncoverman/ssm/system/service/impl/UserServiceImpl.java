package com.uncoverman.ssm.system.service.impl;

import com.uncoverman.ssm.common.util.MD5Utils;
import com.uncoverman.ssm.system.dao.UserMapper;
import com.uncoverman.ssm.system.dao.UserRoleMapper;
import com.uncoverman.ssm.system.domain.User;
import com.uncoverman.ssm.system.domain.UserRole;
import com.uncoverman.ssm.system.domain.UserWithRole;
import com.uncoverman.ssm.system.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserRoleMapper userRoleMapper;

    public List<User> findAllUsers(User user) {
        Example example = new Example(User.class);
        Example.Criteria criteria = example.createCriteria();
        if (user.getUsername() != null && user.getUsername().length()>0) {
            criteria.andCondition("username=", user.getUsername());
        }
        List<User> list = this.userMapper.selectByExample(example);
        return list;
    }

    @Override
    public List<User> findUserWithDept(User user) {
        try {
            return this.userMapper.findUserWithDept(user);
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public User findByName(String username) {
        Example example = new Example(User.class);
        example.createCriteria().andCondition("lower(username)=", username.toLowerCase());
        List<User> list = userMapper.selectByExample(example);
        if (list.size() == 0) {
            return null;
        } else {
            return list.get(0);
        }
    }

    public void addUser(User user, Long[] roleIds) {
        user.setCrateTime(new Date());
        user.setTheme(User.DEFAULT_THEME);
        user.setAvatar(User.DEFAULT_AVATAR);
        user.setPassword(MD5Utils.encrypt(user.getUsername(),user.getPassword()));
        this.userMapper.insert(user);
        for (Long roleId : roleIds){
            UserRole userRole = new UserRole();
            userRole.setUserId(user.getUserId());
            userRole.setRoleId(roleId);
            this.userRoleMapper.insert(userRole);
        }
    }

    public UserWithRole findById(Long userId){
        List<UserWithRole> list = this.userMapper.findUserWithRole(userId);
        if (list.size()==0){
            return null;
        }
        List<Long> roleIds = new ArrayList<>();
        for (UserWithRole uwr : list){
            roleIds.add(uwr.getRoleId());
        }
        UserWithRole userWithRole = list.get(0);
        userWithRole.setRoleIds(roleIds);
        return userWithRole;
    }

    public void updateUser(User user,Long[] roleIds) {
        user.setPassword(null);
        user.setModifyTime(new Date());
        this.userMapper.updateByPrimaryKeySelective(user);
        Example userRoleExample = new Example(UserRole.class);
        userRoleExample.createCriteria().andCondition("user_id=",user.getUserId());
        this.userRoleMapper.deleteByExample(userRoleExample);
        for (Long roleId : roleIds){
            UserRole userRole = new UserRole();
            userRole.setUserId(user.getUserId());
            userRole.setRoleId(roleId);
            this.userRoleMapper.insert(userRole);
        }
    }

    public void deleteusers(String userIds) {
        List<String> list = Arrays.asList(userIds.split(","));
        // 删除t_user_role表数据
        Example userRoleExample = new Example(UserRole.class);
        userRoleExample.createCriteria().andIn("userId",list);
        this.userRoleMapper.deleteByExample(userRoleExample);
        // 删除t_user表数据
        Example userExample = new Example(User.class);
        userExample.createCriteria().andIn("userId",list);
        this.userMapper.deleteByExample(userExample);

    }

    public void registUser(User user) {
        user.setCrateTime(new Date());
        user.setTheme(User.DEFAULT_THEME);
        user.setAvatar(User.DEFAULT_AVATAR);
        user.setSsex(User.SEX_UNKNOW);
        user.setPassword(MD5Utils.encrypt(user.getUsername(), user.getPassword()));
        user.setStatus("0");
        this.userMapper.insert(user);
    }

    @Override
    public void updatePassword(String password) {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        Example example = new Example(User.class);
        example.createCriteria().andCondition("username=", user.getUsername());
        String newPassword = MD5Utils.encrypt(user.getUsername().toLowerCase(), password);
        user.setPassword(newPassword);
        this.userMapper.updateByExampleSelective(user, example);
    }

    @Override
    public User findUserProfile(User user) {
        return this.userMapper.findUserProfile(user);
    }

    @Override
    public void updateUserProfile(User user) {
        this.userMapper.updateByPrimaryKeySelective(user);
    }

}
