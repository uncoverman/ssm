package com.uncoverman.ssm.system.service;

import com.uncoverman.ssm.common.domain.Tree;
import com.uncoverman.ssm.system.domain.Dept;

import java.util.List;

public interface DeptService {

	Tree<Dept> getDeptTree();
	List<Dept> findAllDepts(Dept dept);
	Dept findByName(String deptName);
	Dept findById(Long deptId);
	void addDept(Dept dept);
	void updateDept(Dept dept);
	void deleteDepts(String deptIds);

}
