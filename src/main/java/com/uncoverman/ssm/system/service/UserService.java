package com.uncoverman.ssm.system.service;

import com.uncoverman.ssm.system.domain.User;
import com.uncoverman.ssm.system.domain.UserWithRole;

import java.util.List;

public interface UserService {

    List<User> findAllUsers(User user);
    List<User> findUserWithDept(User user);
    User findByName(String username);
    UserWithRole findById(Long userId);
    void addUser(User user, Long[] roleIds);
    void updateUser(User user, Long[] roleIds);
    void deleteusers(String userIds);
    void registUser(User user);
    void updatePassword(String password);
    User findUserProfile(User user);
    void updateUserProfile(User user);


}
