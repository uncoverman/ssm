package com.uncoverman.ssm.system.service.impl;

import com.uncoverman.ssm.common.domain.Tree;
import com.uncoverman.ssm.common.util.TreeUtils;
import com.uncoverman.ssm.system.dao.MenuMapper;
import com.uncoverman.ssm.system.domain.Menu;
import com.uncoverman.ssm.system.service.MenuService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by NFL on 2018/8/30.
 */
@Service
public class MenuServiceImpl implements MenuService{

    @Autowired
    private MenuMapper menuMapper;

    private void buildTrees(List<Tree<Menu>> trees, List<Menu> menus) {
        for (Menu menu : menus) {
            Tree<Menu> tree = new Tree<>();
            tree.setId(menu.getMenuId().toString());
            tree.setParentId(menu.getParentId().toString());
            tree.setText(menu.getMenuName());
            trees.add(tree);
        }
    }

    public List<Menu> findAllMenus(Menu menu) {
        Example example = new Example(Menu.class);
        Example.Criteria criteria = example.createCriteria();
        if (StringUtils.isNotBlank(menu.getMenuName())){
            criteria.andCondition("menu_name=",menu.getMenuName());
        }
        if(StringUtils.isNotBlank(menu.getType())){
            criteria.andCondition("type=",menu.getType());
        }
        example.setOrderByClause("menu_id");
        return this.menuMapper.selectByExample(example);

    }

    public List<Menu> findByName(String menuName) {
        Example example = new Example(Menu.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andLike("menu_name","%"+menuName+"%");
        return this.menuMapper.selectByExample(example);
    }

    @Override
    public Menu findById(Long menuId) {
        return this.menuMapper.selectByPrimaryKey(menuId);
    }

    @Override
    public List<Menu> findUserPermissions(String username) {
        return this.menuMapper.findUserPermissions(username);
    }

    public void addMenu(Menu menu) {
        if (menu.getParentId() == null){
            menu.setParentId(0l);
        }
        menu.setCreateTime(new Date());
        this.menuMapper.insert(menu);

    }

    public void updateMenu(Menu menu) {
        if (menu.getParentId() == null){
            menu.setParentId(0l);
        }
        menu.setModifyTime(new Date());
        this.menuMapper.updateByPrimaryKeySelective(menu);
    }

    public void deleteMenus(String menuIds) {
        List<String> list = Arrays.asList(menuIds.split(","));
        Example example = new Example(Menu.class);
        example.createCriteria().andIn("menuId",list);
        this.menuMapper.deleteByExample(example);
    }

    public Tree<Menu> getMenuTree() {
        List<Tree<Menu>> trees = new ArrayList<>();
        Example example = new Example(Menu.class);
        example.createCriteria().andCondition("type =", 0);
        example.setOrderByClause("create_time");
        List<Menu> menus = this.menuMapper.selectByExample(example);
        buildTrees(trees, menus);
        return TreeUtils.build(trees);
    }

    @Override
    public Tree<Menu> getMenuButtonTree() {
        List<Tree<Menu>> trees = new ArrayList<>();
        List<Menu> menus = this.findAllMenus(new Menu());
        for (Menu menu : menus){
            Tree<Menu> tree = new Tree<>();
            tree.setId(menu.getMenuId().toString());
            tree.setParentId(menu.getParentId().toString());
            tree.setText(menu.getMenuName().toString());
            trees.add(tree);
        }
        Tree<Menu> t = TreeUtils.build(trees);
        return t;
    }

    public List<Menu> findUserMenus(String username) {
        return this.menuMapper.findUserMenus(username);
    }

    public Tree<Menu> getUserMenu(String username) {
        List<Tree<Menu>> trees = new ArrayList<>();
        List<Menu> menus = this.findUserMenus(username);
        for (Menu menu : menus) {
            Tree<Menu> tree = new Tree<>();
            tree.setId(menu.getMenuId().toString());
            tree.setParentId(menu.getParentId().toString());
            tree.setText(menu.getMenuName());
            tree.setIcon(menu.getIcon());
            tree.setUrl(menu.getUrl());
            trees.add(tree);
        }
        return TreeUtils.build(trees);
    }
}
