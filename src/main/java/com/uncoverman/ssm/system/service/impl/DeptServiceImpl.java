package com.uncoverman.ssm.system.service.impl;

import com.uncoverman.ssm.common.domain.Tree;
import com.uncoverman.ssm.common.service.impl.BaseService;
import com.uncoverman.ssm.common.util.TreeUtils;
import com.uncoverman.ssm.system.dao.DeptMapper;
import com.uncoverman.ssm.system.domain.Dept;
import com.uncoverman.ssm.system.service.DeptService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class DeptServiceImpl extends BaseService<Dept> implements DeptService{

	@Autowired
	private DeptMapper deptMapper;

	@Override
	public Tree<Dept> getDeptTree() {
		List<Tree<Dept>> trees = new ArrayList<>();
		List<Dept> depts = this.findAllDepts(new Dept());
		for (Dept dept : depts){
			Tree<Dept> tree = new Tree<>();
			tree.setId(dept.getDeptId().toString());
			tree.setParentId(dept.getParentId().toString());
			tree.setText(dept.getDeptName().toString());
			trees.add(tree);
		}
		Tree<Dept> t = TreeUtils.build(trees);
		return t;
	}



	@Override
	public List<Dept> findAllDepts(Dept dept) {

		try {
			Example example = new Example(Dept.class);
			Example.Criteria criteria = example.createCriteria();
			if(StringUtils.isNotBlank(dept.getDeptName())){
				criteria.andCondition("dept_name= " , dept.getDeptName());
			}
			example.setOrderByClause("dept_id");
			return deptMapper.selectByExample(example);
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<Dept>();
		}
	}

	@Override
	public Dept findByName(String deptName) {

		Example example = new Example(Dept.class);
		Example.Criteria criteria = example.createCriteria();
		criteria.andCondition("deptName= " , deptName);
		List<Dept> list = deptMapper.selectByExample(example);
		if (list.size() == 0){
			return null;
		}else {
			return list.get(0);
		}
	}

	@Override
	public Dept findById(Long deptId) {
		return this.deptMapper.selectByPrimaryKey(deptId);
	}

	@Override
	public void addDept(Dept dept) {
		Long parentId = dept.getDeptId();
		if (parentId == null){
			dept.setParentId(0l);
		}
		dept.setCreateTime(new Date());
		this.deptMapper.insert(dept);
	}

	@Override
	public void updateDept(Dept dept) {
		this.deptMapper.updateByPrimaryKeySelective(dept);

	}

	@Override
	public void deleteDepts(String deptIds) {
		List<String> list = Arrays.asList(deptIds.split(","));
		Example example = new Example(Dept.class);
		Example.Criteria criteria = example.createCriteria();
		criteria.andIn("deptId",list);
		this.deptMapper.deleteByExample(example);
	}
}
