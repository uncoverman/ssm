package com.uncoverman.ssm.system.service.impl;

import com.uncoverman.ssm.system.dao.DictMapper;
import com.uncoverman.ssm.system.domain.Dict;
import com.uncoverman.ssm.system.service.DictService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.List;

/**
 * Created by NFL on 2018/12/18.
 */
@Service
public class DictServiceImpl implements DictService {

    @Autowired
    private DictMapper dictMapper;

    @Override
    public List<Dict> findAllDicts(Dict dict) {

        Example example = new Example(Dict.class);
        Example.Criteria criteria = example.createCriteria();
//        if (StringUtils.isNotBlank(dict.getKeyy().toString()) ){
//            criteria.andCondition("keyy",dict.getKeyy());
//        }
        if (StringUtils.isNotBlank(dict.getValuee())){
//            criteria.andCondition("valuee=",dict.getValuee());
            criteria.andLike("valuee","%"+dict.getValuee()+"%");
        }
        if (StringUtils.isNotBlank(dict.getFieldName())){
            criteria.andCondition("field_name=",dict.getFieldName());
        }
        if (StringUtils.isNotBlank(dict.getTableName())){
            criteria.andCondition("table_name=",dict.getTableName());
        }
        return this.dictMapper.selectByExample(example);
    }

    @Override
    public void addDict(Dict dict) {
        this.dictMapper.insert(dict);
    }

    @Override
    public void updateDict(Dict dict) {
        this.updateDict(dict);
    }

    @Override
    public void deleteDicts(String dictIds) {
        List<String> list = Arrays.asList(dictIds.split(","));
        Example example = new Example(Dict.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andIn("dictId",list);
        this.dictMapper.deleteByExample(example);
    }
}
