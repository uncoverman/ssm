package com.uncoverman.ssm.system.service.impl;

import com.uncoverman.ssm.system.dao.RoleMapper;
import com.uncoverman.ssm.system.dao.RoleMenuMapper;
import com.uncoverman.ssm.system.domain.Role;
import com.uncoverman.ssm.system.domain.RoleMenu;
import com.uncoverman.ssm.system.domain.RoleWithMenu;
import com.uncoverman.ssm.system.service.RoleService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.*;

/**
 * Created by NFL on 2018/9/28.
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private RoleMenuMapper roleMenuMapper;

    public List<Role> findAllRoles(Role role) {
        Example example = new Example(Role.class);
        if (StringUtils.isNotBlank(role.getRoleName())){
            example.createCriteria().andCondition("role_name=",role.getRoleName());
        }
        example.setOrderByClause("create_time");
        return roleMapper.selectByExample(example);
    }

    public List<Role> findByName(String roleName) {
        Example example = new Example(Role.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andLike("roleName","%"+roleName+"%");
        List<Role> list = roleMapper.selectByExample(example);
        if (list.size() == 0) {
            return null;
        } else {
            return list;
        }
    }

    @Override
    public List<Role> findUserRole(String username) {
        return this.roleMapper.findUserRole(username);
    }

    @Override
    public RoleWithMenu findRoleWithMenus(Long roleId) {

        List<RoleWithMenu> list = this.roleMapper.findById(roleId);
        if (list.size()==0){
            return null;
        }
        List<Long> menuList = new ArrayList<>();
        for (RoleWithMenu rwm :list){
            menuList.add(rwm.getMenuId());
        }
        RoleWithMenu roleWithMenu = list.get(0);
        roleWithMenu.setMenuIds(menuList);
        return roleWithMenu;

    }

    public void addRole(Role role, Long[] menuIds) {
        role.setCreateTime(new Date());
        roleMapper.insert(role);
        for (Long menuId : menuIds){
            RoleMenu roleMenu = new RoleMenu();
            roleMenu.setRoleId(role.getRoleId());
            roleMenu.setMenuId(menuId);
            this.roleMenuMapper.insert(roleMenu);
        }

    }

    public void updateRole(Role role, Long[] menuIds) {
        role.setModifyTime(new Date());
        roleMapper.updateByPrimaryKeySelective(role);
        Example example = new Example(RoleMenu.class);
        example.createCriteria().andCondition("role_id=",role.getRoleId());
        this.roleMenuMapper.deleteByExample(example);
        for (Long menuId : menuIds){
            RoleMenu roleMenu = new RoleMenu();
            roleMenu.setRoleId(role.getRoleId());
            roleMenu.setMenuId(menuId);
            this.roleMenuMapper.insert(roleMenu);
        }
    }

    public void deleteRoles(String roleIds) {
        List<String> list = Arrays.asList(roleIds.split(","));

        Example roleMenuExample = new Example(RoleMenu.class);
        roleMenuExample.createCriteria().andIn("roleId",list);
        this.roleMenuMapper.deleteByExample(roleMenuExample);

        Example roleExample = new Example(Role.class);
        roleExample.createCriteria().andIn("roleId",list);
        this.roleMapper.deleteByExample(roleExample);

    }
}
