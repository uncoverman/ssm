package com.uncoverman.ssm.system.service;

import com.uncoverman.ssm.system.domain.Dict;

import java.util.List;

/**
 * Created by NFL on 2018/12/18.
 */
public interface DictService {

    List<Dict> findAllDicts(Dict dict);
    void addDict(Dict dict);
    void updateDict(Dict dict);
    void deleteDicts(String dicts);
}
