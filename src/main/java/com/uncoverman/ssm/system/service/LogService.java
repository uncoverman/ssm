package com.uncoverman.ssm.system.service;

import com.uncoverman.ssm.system.domain.SysLog;

import java.util.List;

/**
 * Created by NFL on 2018/12/17.
 */
public interface LogService {

    List<SysLog> findAllLogs(SysLog log);

    void deleteLogs(String logIds);

}
