package com.uncoverman.ssm.system.service;

import com.uncoverman.ssm.common.domain.Tree;
import com.uncoverman.ssm.system.domain.Menu;

import java.util.List;

/**
 * Created by NFL on 2018/8/30.
 */
public interface MenuService {

    List<Menu> findAllMenus(Menu menu);
    List<Menu> findByName(String menuName);
    List<Menu> findUserPermissions(String username);
    Menu findById(Long menuId);
    void addMenu(Menu menu);
    void updateMenu(Menu menu);
    void deleteMenus(String menuIds);
    Tree<Menu> getMenuTree();
    Tree<Menu> getUserMenu(String username);
    Tree<Menu> getMenuButtonTree();
    List<Menu> findUserMenus(String username);

}
