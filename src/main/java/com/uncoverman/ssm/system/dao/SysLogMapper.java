package com.uncoverman.ssm.system.dao;

import com.uncoverman.ssm.system.domain.SysLog;
import tk.mybatis.mapper.common.Mapper;

public interface SysLogMapper extends Mapper<SysLog> {
}