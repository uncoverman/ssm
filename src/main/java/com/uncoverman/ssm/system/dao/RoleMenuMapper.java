package com.uncoverman.ssm.system.dao;

import com.uncoverman.ssm.system.domain.RoleMenu;
import tk.mybatis.mapper.common.Mapper;

public interface RoleMenuMapper extends Mapper<RoleMenu> {
}