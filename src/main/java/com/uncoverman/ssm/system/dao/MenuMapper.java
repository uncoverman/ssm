package com.uncoverman.ssm.system.dao;

import com.uncoverman.ssm.system.domain.Menu;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface MenuMapper extends Mapper<Menu> {

    /**
     * 查找用户菜单
     * 备注：手工添加
     */

    List<Menu> findUserMenus(String username);
    List<Menu> findUserPermissions(String username);

}