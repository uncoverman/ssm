package com.uncoverman.ssm.system.dao;

import com.uncoverman.ssm.system.domain.Dept;
import tk.mybatis.mapper.common.Mapper;

public interface DeptMapper extends Mapper<Dept> {
}