package com.uncoverman.ssm.system.dao;

import com.uncoverman.ssm.system.domain.Role;
import com.uncoverman.ssm.system.domain.RoleWithMenu;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface RoleMapper extends Mapper<Role> {
	/**
	 * 通过roleId获取role及menu，手工添加
	 * @param roleId
	 * @return
	 */
	List<RoleWithMenu> findById(Long roleId);

	List<Role> findUserRole(String username);
}