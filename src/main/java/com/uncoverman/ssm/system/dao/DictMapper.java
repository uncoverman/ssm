package com.uncoverman.ssm.system.dao;

import com.uncoverman.ssm.system.domain.Dict;
import tk.mybatis.mapper.common.Mapper;

public interface DictMapper extends Mapper<Dict> {
}