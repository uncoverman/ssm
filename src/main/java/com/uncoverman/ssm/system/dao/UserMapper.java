package com.uncoverman.ssm.system.dao;

import com.uncoverman.ssm.system.domain.User;
import com.uncoverman.ssm.system.domain.UserWithRole;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface UserMapper extends Mapper<User> {

    List<User> findUserWithDept(User user);
    List<UserWithRole> findUserWithRole(Long userId);
    User findUserProfile(User user);
}