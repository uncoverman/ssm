package com.uncoverman.ssm.system.dao;

import com.uncoverman.ssm.system.domain.UserRole;
import tk.mybatis.mapper.common.Mapper;

public interface UserRoleMapper extends Mapper<UserRole> {
}