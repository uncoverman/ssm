package com.uncoverman.ssm.system.controller;

import com.uncoverman.ssm.common.annotation.Log;
import com.uncoverman.ssm.common.controller.BaseController;
import com.uncoverman.ssm.common.domain.ResponseBo;
import com.uncoverman.ssm.common.domain.Tree;
import com.uncoverman.ssm.common.util.FileUtils;
import com.uncoverman.ssm.system.domain.Dept;
import com.uncoverman.ssm.system.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class DeptController extends BaseController {

	@Autowired
	DeptService deptService;

	@Log("获取部门信息")
	@RequestMapping("dept")
	public String index(){
		return "system/dept/dept";
	}

	@RequestMapping("dept/tree")
	@ResponseBody
	public ResponseBo deptTree(){
		try {
			Tree<Dept> tree = this.deptService.getDeptTree();
			return ResponseBo.ok(tree);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error("获取部门失败！");
		}
	}

	@RequestMapping("dept/list")
	@ResponseBody
	public List<Dept> deptList(Dept dept){
		try {
			return this.deptService.findAllDepts(dept);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@RequestMapping("dept/getDept")
	@ResponseBody
	public ResponseBo getDept(Long deptId){
		Dept dept = this.deptService.findById(deptId);
		return ResponseBo.ok(dept);
	}

	@Log("新增部门 ")
	@RequestMapping("dept/add")
	@ResponseBody
	public ResponseBo addDept(Dept dept){
		try {
			this.deptService.addDept(dept);
			return ResponseBo.ok("新增部门成功！");
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error("新增部门失败！");
		}
	}

	@Log("修改部门 ")
	@RequestMapping("dept/update")
	@ResponseBody
	public ResponseBo updateDept(Dept dept){
		try {
			this.deptService.updateDept(dept);
			return ResponseBo.ok("修改部门成功！");
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error("修改部门失败！");
		}
	}

	@Log("删除部门 ")
	@RequestMapping("dept/delete")
	@ResponseBody
	public ResponseBo deleteDepts(String ids){
		try {
			this.deptService.deleteDepts(ids);
			return ResponseBo.ok("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error("删除失败");
		}
	}

	@RequestMapping("dept/csv")
	@ResponseBody
	public ResponseBo deptCsv(Dept dept){
		try {
			List<Dept> list = this.deptService.findAllDepts(dept);
			return FileUtils.createCsv("test",list,Dept.class);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error("导出CSV失败！");
		}
	}

}
