package com.uncoverman.ssm.system.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uncoverman.ssm.common.annotation.Log;
import com.uncoverman.ssm.common.controller.BaseController;
import com.uncoverman.ssm.common.domain.QueryRequest;
import com.uncoverman.ssm.common.domain.ResponseBo;
import com.uncoverman.ssm.system.domain.Dict;
import com.uncoverman.ssm.system.service.DictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * Created by NFL on 2018/12/18.
 */

@Controller
public class DictController extends BaseController {
    @Autowired
    private DictService dictService;

    @RequestMapping("dict")
    public String index(){
        return "system/dict/dict";
    }

    @Log("获取字典信息")
    @RequestMapping("dict/list")
    @ResponseBody
    public Map<String, Object> dictList(QueryRequest request,Dict dict){
        PageHelper.startPage(request.getPageNum(),request.getPageSize());
        List<Dict> list = this.dictService.findAllDicts(dict);
        PageInfo<Dict> pageInfo = new PageInfo<>(list);
        return getDataTable(pageInfo);
    }


    @Log("新增字典 ")
    @RequestMapping("dict/add")
    @ResponseBody
    public ResponseBo addDict(Dict dict){
        try {
            this.dictService.addDict(dict);
            return ResponseBo.ok("新增成功！");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBo.error("新增失败！");
        }
    }

    @Log("修改字典 ")
    @RequestMapping("dict/update")
    @ResponseBody
    public ResponseBo updateDict(Dict dict){
        try {
            this.dictService.updateDict(dict);
            return ResponseBo.ok("修改成功！");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBo.error("修改失败！");
        }
    }

    @Log("删除字典 ")
    @RequestMapping("dict/delete")
    @ResponseBody
    public ResponseBo deleteDicts(String ids){
        try {
            this.dictService.deleteDicts(ids);
            return ResponseBo.ok("删除成功！");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBo.error("删除失败！");
        }
    }

}


