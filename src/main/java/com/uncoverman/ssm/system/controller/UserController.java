package com.uncoverman.ssm.system.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uncoverman.ssm.common.annotation.Log;
import com.uncoverman.ssm.common.controller.BaseController;
import com.uncoverman.ssm.common.domain.QueryRequest;
import com.uncoverman.ssm.common.domain.ResponseBo;
import com.uncoverman.ssm.system.domain.User;
import com.uncoverman.ssm.system.domain.UserWithRole;
import com.uncoverman.ssm.system.service.UserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
public class UserController extends BaseController{

	@Autowired
	private UserService userService;

	private static final String ON = "on";

	@RequestMapping("user")
	public String index(Model model) {
		User user = super.getCurrentUser();
		model.addAttribute("user", user);
		return "system/user/user";
	}

	@RequestMapping("user/list")
	@RequiresPermissions("user:list")
	@ResponseBody
	public Map<String, Object> userList(QueryRequest request, User user){
		PageHelper.startPage(request.getPageNum(),request.getPageSize());
		List<User> list = this.userService.findUserWithDept(user);
		PageInfo<User> pageInfo = new PageInfo<>(list);
		return getDataTable(pageInfo);
	}

	@RequestMapping("user/regist")
	@ResponseBody
	public ResponseBo regist(User user) {
		try {
			User result = this.userService.findByName(user.getUsername());
			if (result != null) {
				return ResponseBo.warn("该用户名已被使用！");
			}
			this.userService.registUser(user);
			return ResponseBo.ok();
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error("注册失败，请联系网站管理员！");
		}
	}

	@Log("新增用户")
	@RequiresPermissions("user:add")
	@RequestMapping("user/add")
	@ResponseBody
	public ResponseBo addUser(User user,Long[] roles){
		try {
			if (ON.equalsIgnoreCase(user.getStatus())){
				user.setStatus(User.STATUS_VALID);
			}else {
				user.setStatus(User.STATUS_LOCK);
			}
			this.userService.addUser(user,roles);
			return ResponseBo.ok("新增成功！");
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error("新增失败！");
		}
	}

	@RequestMapping("user/getUser")
	@ResponseBody
	public ResponseBo getUser(Long userId){
		try {
			UserWithRole userWithRole = this.userService.findById(userId);
			return ResponseBo.ok(userWithRole);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error("获取用户失败");
		}
	}

	@RequestMapping("user/update")
	@RequiresPermissions("user:update")
	@ResponseBody
	public ResponseBo updateUser(User user,Long[] roles){
		try {
			if (ON.equalsIgnoreCase(user.getStatus())){
				user.setStatus(User.STATUS_VALID);
			}else {
				user.setStatus(User.STATUS_LOCK);
			}
			this.userService.updateUser(user,roles);
			return ResponseBo.ok("修改成功！");
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error("修改失败！");
		}
	}

	@RequestMapping("user/delete")
	@ResponseBody
	public ResponseBo deleteUsers(String ids){
		try {
			this.userService.deleteusers(ids);
			return ResponseBo.ok("删除成功！");
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error("删除失败！");
		}
	}

	@RequestMapping("user/updatePassword")
	@ResponseBody
	public ResponseBo updatePassword(String newPassword) {
		try {
			this.userService.updatePassword(newPassword);
			return ResponseBo.ok("更改密码成功！");
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error("更改密码失败！");
		}
	}

	@RequestMapping("user/profile")
	public String profileIndex(Model model) {
		User user = super.getCurrentUser();
		user = this.userService.findUserProfile(user);
		String ssex = user.getSsex();
		if (User.SEX_MALE.equals(ssex)) {
			user.setSsex("性别：男");
		} else if (User.SEX_FEMALE.equals(ssex)) {
			user.setSsex("性别：女");
		} else {
			user.setSsex("性别：保密");
		}
		model.addAttribute("user", user);
		return "system/user/profile";
	}

	@RequestMapping("user/getUserProfile")
	@ResponseBody
	public ResponseBo getUserProfile(Long userId) {
		try {
			User user = new User();
			user.setUserId(userId);
			return ResponseBo.ok(this.userService.findUserProfile(user));
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error("获取用户信息失败，请联系网站管理员！");
		}
	}

	@RequestMapping("user/updateUserProfile")
	@ResponseBody
	public ResponseBo updateUserProfile(User user) {
		try {
			this.userService.updateUserProfile(user);
			return ResponseBo.ok("更新个人信息成功！");
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error("更新用户信息失败，请联系网站管理员！");
		}
	}


	// method不写，默认支持get、post等所有方式
//	@RequestMapping(value = "/login",method = RequestMethod.POST)
//	@ResponseBody
//	public ResponseBo login(String username, String password,String code,Boolean remeberMe){
//
//		return ResponseBo.ok();
//	}

}

