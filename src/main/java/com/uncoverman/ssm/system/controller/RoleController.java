package com.uncoverman.ssm.system.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uncoverman.ssm.common.annotation.Log;
import com.uncoverman.ssm.common.controller.BaseController;
import com.uncoverman.ssm.common.domain.QueryRequest;
import com.uncoverman.ssm.common.domain.ResponseBo;
import com.uncoverman.ssm.system.domain.Role;
import com.uncoverman.ssm.system.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * Created by NFL on 2018/9/28.
 */
@Controller
public class RoleController extends BaseController{

    @Autowired
    private RoleService roleService;

    @RequestMapping("role")
    public String index(){
        return "system/role/role";
    }

    @Log("获取角色信息")
    @RequestMapping(value = "role/list")
    @ResponseBody
    public Map<String,Object> roleList(QueryRequest request, Role role) {
        PageHelper.startPage(request.getPageNum(),request.getPageSize());
        List<Role> list = this.roleService.findAllRoles(role);
        PageInfo<Role> pageInfo = new PageInfo<>(list);
        return getDataTable(pageInfo);
    }

    @RequestMapping("role/getRole")
    @ResponseBody
    public ResponseBo getRole(Long roleId){
        return ResponseBo.ok(this.roleService.findRoleWithMenus(roleId));
    }

    @Log("新增角色 ")
    @RequestMapping("role/add")
    @ResponseBody
    public ResponseBo addRole(Role role, Long[] menuId){
        try {
            this.roleService.addRole(role,menuId);
            return ResponseBo.ok("新增成功！");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBo.error("新增失败！");
        }
    }

    @Log("修改角色 ")
    @RequestMapping("role/update")
    @ResponseBody
    public ResponseBo updateRole(Role role, Long[] menuId){
        try {
            this.roleService.updateRole(role,menuId);
            return ResponseBo.ok("修改成功！");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBo.error("修改失败！");
        }
    }

    @Log("删除角色 ")
    @RequestMapping("role/delete")
    @ResponseBody
    public ResponseBo deleteRoles(String ids){
        try {
            this.roleService.deleteRoles(ids);
            return ResponseBo.ok("删除成功！");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBo.error("删除失败！");
        }
    }

}
