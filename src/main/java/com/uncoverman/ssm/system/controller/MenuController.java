package com.uncoverman.ssm.system.controller;

import com.uncoverman.ssm.common.domain.ResponseBo;
import com.uncoverman.ssm.common.domain.Tree;
import com.uncoverman.ssm.system.domain.Menu;
import com.uncoverman.ssm.system.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NFL on 2018/8/31.
 */
@Controller
public class MenuController {
	@Autowired
	private MenuService menuService;

	@RequestMapping("menu")
	public String index(){
		return "system/menu/menu";
	}

	@RequestMapping("menu/list")
	@ResponseBody
	public List<Menu> menuList(Menu menu) {
		try {
			List<Menu> list = this.menuService.findAllMenus(menu);
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<Menu>();
		}
	}

	@RequestMapping("menu/getUserMenu")
	@ResponseBody
	public ResponseBo getUserMenu(String username) {
		try {
			Tree<Menu> tree = this.menuService.getUserMenu(username);
			return ResponseBo.ok(tree);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error("获取用户菜单失败！");
		}
	}

    @RequestMapping("menu/tree")
    @ResponseBody
    public ResponseBo menuTree(){
        try {
            Tree<Menu> tree = this.menuService.getMenuTree();
            return ResponseBo.ok(tree);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBo.error("获取菜单失败");
        }
    }

    @RequestMapping("menu/menuButtonTree")
	@ResponseBody
	public ResponseBo getMenuButtonTree(){
		try {
			return ResponseBo.ok(this.menuService.getMenuButtonTree());
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error("获取菜单列表失败！");
		}
	}

	@RequestMapping("menu/add")
	@ResponseBody
	public ResponseBo addMenu(Menu menu){
		try {
			this.menuService.addMenu(menu);
			return ResponseBo.ok("新增成功！");
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error("新增失败！");
		}
	}

	@RequestMapping("menu/getMenu")
	@ResponseBody
	public ResponseBo getMenu(Long menuId){
		try {
			Menu menu = this.menuService.findById(menuId);
			return ResponseBo.ok(menu);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseBo.error("获取菜单信息失败！");
		}
	}

    @RequestMapping("menu/update")
    @ResponseBody
    public ResponseBo updateMenu(Menu menu){
        try {
            this.menuService.updateMenu(menu);
            return ResponseBo.ok("修改成功！");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBo.error("修改失败");
        }
    }

    @RequestMapping("menu/delete")
    @ResponseBody
    public ResponseBo deleteMenus(String ids){
        try {
            this.menuService.deleteMenus(ids);
            return ResponseBo.ok("删除成功！");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBo.error("删除失败");
        }
    }

}
