package com.uncoverman.ssm.system.domain;

import java.util.List;

/**
 * Created by NFL on 2018/12/24.
 */
public class UserWithRole extends User {
    private Long roleId;
    private List<Long> roleIds;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public List<Long> getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(List<Long> roleIds) {
        this.roleIds = roleIds;
    }
}
